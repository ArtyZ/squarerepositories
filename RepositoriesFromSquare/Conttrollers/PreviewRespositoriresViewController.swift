//
//  PreviewRespositoriresViewController.swift
//  RepositoriesFromSquare
//
//  Created by Artur-Mac on 24/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import UIKit

class PreviewRespositoriresViewController: UIViewController {
    var respositoriresData: RespositoriesResponse?
    
    @IBOutlet var labelCollection: [UILabel]!
    
    override func viewDidLoad() {
        labelCollection[0].text = respositoriresData?.fullName
        labelCollection[1].text = respositoriresData?.description
    }
}
