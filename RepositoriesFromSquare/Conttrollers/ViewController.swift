//
//  ViewController.swift
//  RepositoriesFromSquare
//
//  Created by Artur-Mac on 22/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import UIKit

let kNumberOfItemOnList = 20

class ViewController: UIViewController {
    
    enum cellIdentifiers {
        static let list = "list"
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private var viewModel: RespositoriesViewModel!
    private var heightOfCell: CGFloat = 100
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RespositoriesViewModel(numberOfItems: kNumberOfItemOnList, delegate: self)
        viewModel.getRespositories()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PreviewSegueIdentifier" {
            if let destinationVC = segue.destination as? PreviewRespositoriresViewController, let data = sender as? RespositoriesResponse {
                destinationVC.respositoriresData = data
            }
        }
    }
}

extension ViewController: RespositoriesViewModelDelegate, ErrorPresenting {
    func getFiltered() {
        collectionView.reloadSections(IndexSet(integersIn: 0...0))
    }
    
    func getRespositoriesCompleted() {
        collectionView.reloadData()
    }
    
    func getRespositorieshFailedWith(error: Error) {
        presentError(error)
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.filteredCount
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifiers.list, for: indexPath) as! ListRespositoriesCollectionCell
        let item = viewModel.filteredRepos(at: indexPath.item)
        cell.nameLabel.text = item.name
        cell.languageLabel.text = item.language ?? ""
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "PreviewSegueIdentifier",  sender: viewModel.repos(at: indexPath.item))
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: heightOfCell)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return .leastNonzeroMagnitude
    }
}

extension ViewController: UISearchBarDelegate {
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        viewModel.filteredRepos(by: "")
        searchBar.resignFirstResponder()
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filteredRepos(by: searchText)
    }
}


