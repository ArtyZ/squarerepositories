//
//  RespositoriesResponse.swift
//  RepositoriesFromSquare
//
//  Created by Artur-Mac on 24/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import Foundation

struct RespositoriesResponse: Decodable {
    let id: Int
    let name: String
    let fullName: String
    var description: String?
    var language: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case fullName = "full_name"
    }
}

