//
//  ListRespositoriesCollectionCell.swift
//  RepositoriesFromSquare
//
//  Created by Artur-Mac on 24/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import UIKit

class ListRespositoriesCollectionCell: UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
}
