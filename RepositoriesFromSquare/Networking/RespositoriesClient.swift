//
//  RespositoriesClient.swift
//  RepositoriesFromSquare
//
//  Created by Artur-Mac on 24/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import Foundation

enum ResultApi<Element> {
    case failure(Error)
    case success(Element)
}

final class RespositoriesClient {
    var baseURL: String = "https://api.github.com/orgs/square/repos"
    let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func getRespositories(completion: @escaping((ResultApi<[RespositoriesResponse]>) -> Void)) {
        
        let urlRequest = URLRequest(url: URL(string: baseURL)!)
        
        self.session.dataTask(with: urlRequest) { (data, response, error) in
            guard let httpUrlResponse = response as? HTTPURLResponse, httpUrlResponse.statusCode == 200, let data = data else {
                return completion(ResultApi.failure(ApiError.serverError))
            }
            
            guard let decodeData = try? JSONDecoder().decode([RespositoriesResponse].self, from: data) else {
                return completion(ResultApi.failure(ApiError.other(message: "JSONDecoder failed")))
            }
            
            completion(ResultApi.success(decodeData))
            }.resume()
    }
}
