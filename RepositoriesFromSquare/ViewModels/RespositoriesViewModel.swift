//
//  ViewModel.swift
//  RepositoriesFromSquare
//
//  Created by Artur-Mac on 24/10/2018.
//  Copyright © 2018 Artur-Mac. All rights reserved.
//

import Foundation

protocol RespositoriesViewModelDelegate: class {
    func getRespositoriesCompleted()
    func getRespositorieshFailedWith(error: Error)
    func getFiltered()
}

final class RespositoriesViewModel {
    private weak var delegate: RespositoriesViewModelDelegate?
    
    private var repos: [RespositoriesResponse] = [] {
        willSet(newList) {
            
            var result: [RespositoriesResponse] = []
            if newList.isEmpty {
                filteredRepos = result
                self.repos = result
                return
            }
            for item in 0..<numberOfRepos {
                
                result.append(newList[item])
            }
            filteredRepos = result
            self.repos = result
        }
    }
    private var filteredRepos: [RespositoriesResponse] = []
    
    let numberOfRepos: Int
    var currentCount: Int {
        return repos.count
    }
    
    func repos(at index: Int) -> RespositoriesResponse {
        return repos[index]
    }
    var filteredCount: Int {
        return filteredRepos.count
    }
    
    func filteredRepos(at index: Int) -> RespositoriesResponse {
        return filteredRepos[index]
    }
    let client = RespositoriesClient()
    
    init(numberOfItems: Int, delegate: RespositoriesViewModelDelegate) {
        self.delegate = delegate
        self.numberOfRepos = numberOfItems
    }
    
    func getRespositories() {
        client.getRespositories { (result) in
            switch result {
            case .failure(let error):
                DispatchQueue.main.async {
                    self.delegate?.getRespositorieshFailedWith(error: error)
                }
            case .success(let responose):
                self.repos = responose
                DispatchQueue.main.async {
                    self.delegate?.getRespositoriesCompleted()
                }
            }
        }
    }
    
    func filteredRepos(by name: String ) {
        var result: [RespositoriesResponse]
        if name.isEmpty {
            result = repos
            filteredRepos = repos
            delegate?.getFiltered()
            return
        }
        
        let myRange = name.startIndex..<name.endIndex
        result = repos.filter {
            $0.name.count >= name.count && $0.name.range(of: name, options: String.CompareOptions.caseInsensitive, range: myRange, locale: nil) != nil
        }
        filteredRepos = result
        delegate?.getFiltered()
        return
    }
}
